import _ from "lodash";
import Promise from "bluebird";

import initBundle from "interlock/lib/compile/bundles/init";

export default function (opts = {}) {
  const {
    exclude = /^[]/
  } = opts;

  return (override, transform) => {
    transform("getUrls", function (hashToUrlMap) {
      // TODO: Filter out any modules that are loaded with H2 runtime provider. In
      //       simple case, this will result in empty object.  In complex cases,
      //       where a filter is applied or certain objects will not use H2 profile,
      //       ensure those URLs are still contained within the hashToUrl map.
      return hashToUrlMap;
    });

    override("partitionBundles", function (moduleSeeds, moduleMaps) {
      const entryBundleDefs = _.chain(this.opts.entry)
        .map((bundleDef, relPath) => [moduleSeeds[relPath].path, bundleDef])
        .object()
        .value();

      const h2bundles = Promise.all(Object.keys(moduleMaps.byAbsPath)
        .filter(absPath => !exclude.test(absPath))
        .map(absPath => {
          const module = moduleMaps.byAbsPath[absPath];
          const entryBundleDef = entryBundleDefs[module.path] || {};
          const isEntryPt = !!entryBundleDef;

          return initBundle.call(this, Object.assign({
            module,
            isEntryPt,
            moduleHashes: [module.hash]
          }, entryBundleDef));
        }));

      // getBundleSeeds, dedupeExplicit, dedupeImplicit
    });

    // TODO: Override `updateRequires` step, prepending "m:" (signifying module vs
    //       normal bundle behavior) in front of module hash for all module targets
    //       that are to be loaded using HTTP/2 profile.

    // TODO: Transform `constructRuntime` to add additional provider that uses
    //       opts.urlTemplate (from build-time plugin options) to evaluate URL
    //       for loading module. Should only be invoked if `/m\:.+/.test(moduleHash)`.
    //       
    //       Provider should go AFTER the default provider, as if a bundle resource
    //       is defined in the module-to-url resource hash, we should use that.  If that
    //       fails, attempt to load the HTTP/2 resource by name.  And if THAT fails,
    //       throw the error that would normally be thrown (will have to rely on script-
    //       load error).
  };
}
